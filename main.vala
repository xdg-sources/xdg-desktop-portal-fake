/*Empty dbus services */

[DBus (name = "org.freedesktop.portal.Documents")]
public class Documents : Object {
    public int version = 9999;
}

[DBus (name = "org.freedesktop.portal.Desktop")]
public class Desktop : Object {
    public int version = 9999;
}

[DBus (name = "org.freedesktop.portal.Flatpak")]
public class Fatpak : Object {
    public int version = 9999;
}


void on_bus_aquired (DBusConnection conn) {
    try {
        conn.register_object ("/org/freedesktop/portal/documents", new Documents ());
    } catch (IOError e) {}
    try {
        conn.register_object ("/org/freedesktop/portal/desktop", new Desktop ());
    } catch (IOError e) {}
    try {
        conn.register_object ("/org/freedesktop/portal/Flatpak", new Fatpak ());
    } catch (IOError e) {}
}

void main () {
    string[] srvs = {};
    srvs += "org.freedesktop.portal.Documents";
    srvs += "org.freedesktop.portal.Desktop";
    srvs += "org.freedesktop.portal.FileChooser";
    srvs += "org.freedesktop.portal.Flatpak";
    srvs += "org.freedesktop.portal.Settings";
    foreach (string svc in srvs){
        Bus.own_name (BusType.SESSION, svc, BusNameOwnerFlags.NONE,
            on_bus_aquired, () => {}, () => {});

    }
    new MainLoop ().run ();
}


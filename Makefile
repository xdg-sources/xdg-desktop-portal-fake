build:
	valac --pkg gio-2.0 main.vala -o main
install:
	install -D main $(DESTDIR)/usr/bin/fake-portal
	install -D fake-portal.desktop /etc/xdg/autostart/fake-portal.desktop
